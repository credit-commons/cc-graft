<?php

/**
 * A layer of error handling on the node requester.
 */
class TrunkwardAccount extends \CreditCommons\Account implements  \CreditCommons\AccountRemoteInterface {

  /**
   * Get the last hash pertaining to this account.
   *
   * @note If keeping the hashes in a text file like this, the file should be
   * outside of the web root!
   */
  function getLastHash() : string {
    $hashes = file('hashes.txt');
    return array_pop($hashes);
  }

  function saveHash($last_hash) {
    file_put_contents('hashes.txt', "\n",$last_hash, FILE_APPEND);
  }

  /**
   * Check if this Account points to a remote account, rather than a remote node.
   *
   * @return bool
   *   TRUE if this object references a remote account, not a whole node
   *
   * @todo refactor Address resolver so this isn't necessary in Entry::upcastAccounts
   */
  public function isNode() : bool {
    return TRUE;
  }

  /**
   * @return string
   *   'ok' or the class name of the error
   */
  function handshake() : string {

  }
}