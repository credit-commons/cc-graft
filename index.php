<?php
const TRUNKWARD_NODE = 'cctrunk';
const TRUNKWARD_URL = 'http://cctrunk';
const EXCHANGE_RATE = 60;
const NODE_NAME = 'standalone';
const ACC_ID = 'test_user';
const WORKFLOW_ID = 'bill'; // To see workflow ids call TRUNKWARD_URL/workflows
const REDIRECT_URL = '';

$desc_class = $quant_class = $secondparty_class = '';// form field html classes (for errors)
ini_set('display_errors', TRUE);
require_once('TrunkwardAccount.php');
$trunkward_account = new TrunkwardAccount(TRUNKWARD_NODE, 0, 0); // the zeros here are the min/max balances

?><!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <title>Remote bill form</title>
    <style>form input.error{border: thin solid red;}</style>
  </head>
  <body>
  <?php
/**
 * Check authentication...
 */
// This script is in three main parts. The transaction form is at the bottom,
// but first we process the $_POST from the form build the confirmation page,
// then we process the $_POST from the confirmation page, and redirect elsewhere.
// Handle the transaction request and show and confirmation page.
if (!empty($_POST['quant']) and !empty($_POST['description']) and !empty($_POST['2ndparty'])) {
  // Field validation is done in html.
  require_once './vendor/autoload.php';
  \CreditCommons\ErrorContext::Create(user: ACC_ID, node: NODE_NAME);
  $requester = new \CreditCommons\Leaf\LeafRequester(TRUNKWARD_URL, NODE_NAME, $trunkward_account->getLastHash());
  $workflows = new \CreditCommons\Workflows($requester);
  // In this example the current user credits the remote account.
  // Remember that the user cannot create a credit transaction in which he is the payee
  // of a bill transaction in which he is the payer.
  $data = (object)[
    'payer' => $_POST['2ndparty'], // the remote user.
    'payee' => 'standalone/' . ACC_ID, // the current user.
    'type' => WORKFLOW_ID,
    'description' => $_POST['description'],
    'quant' => $_POST['quant']
  ];
  $new_transaction = \CreditCommons\NewTransaction::create($data, $workflows, ACC_ID);
  $transaction = \CreditCommons\Leaf\TransversalTransaction::createFromNew($new_transaction);
  try {
    $remote_transaction = clone($transaction);
    foreach ($remote_transaction->entries() as &$entry) {
      $entry->qaunt *= EXCHANGE_RATE; // multiply exchange rate when sending trunkwards
    }
    $extra_rows = $requester->buildValidateRelayTransaction($remote_transaction);
    foreach ($extra_rows as $row) {
      $row->quant /= EXCHANGE_RATE; // Divide exchange rate for quants coming from trunkwards
      $transaction->entries[] = CreditCommons\EntryFlat::create($row);
    }
  }
  catch (\CreditCommons\Exceptions\SameAccountViolation $e) {
    $message = $e->getMessage();
    $secondparty_class = 'error';
  }
  // there might be other exception types here.
  catch (\CreditCommons\Exceptions\CCError $e) {// this covers all other exceptions.
    // Put the message on the form $e->getMessage() and reload the form
    $message = get_class($e) .': '. $e->getMessage();
    $desc_class = $quant_class = $secondparty_class = 'error'; // make some css for this.
    // Log the error so the admin can see it.
    mail(ADMIN_MAIL, 'Credit Commons client error',  print_r($e));
  }
  /**
   * At this point the transaction should be saved in an uncomfirmed state,
   * so that we can access it in the next step via its uuid...
   * @note In the local version of the transaction the remote user (payee)
   * should be replaced by the exchange's balance of trade account. The name of
   * the counterparty can be stored seperately and retrieved when the
   * transaction is displayed.
   */
  // Show the confirmation form.
  if (empty($message)) {
    // Render the transaction to screen with a confirm button.
    print $transaction->render();
    $hash = $transaction->makeHash($trunkward_account);
    $trunkward_account->saveHash($hash);
    // The 'state' would normally be derived from the workflow, which isn't available here.
    print '<p><form>
    <input type="hidden" name="hash" value="'.$hash.'">
    <input type="hidden" name="uuid" value="'.$transaction->uuid.'">
    <input type="submit" name="confirm" value="Confirm">
    </form></p>';
    // Probably it's bad security to put the hash as a form value, but we do it
    // here so as not to need to load the transaction from storage in the next step.
  }
}
// Confirm the transaction and redirect
elseif (!empty($_POST['confirm'])) {
  require_once './vendor/autoload.php';
  \CreditCommons\ErrorContext::Create(user: ACC_ID, node: NODE_NAME);
  $requester = new \CreditCommons\LeafRequester(TRUNKWARD_URL, NODE_NAME, $last_hash);
  try {
    $requester->transactionChangeState($_POST['uuid'], $_POST['state']);
    // No response.

    /**
     * Update the saved transaction with the state 'finished'.
     * Save the hash $_POST['hash']
     */

    if (REDIRECT_URL) {
      // This provides no way of putting a confirmation message on the next page.
      header('Location: '.REDIRECT_URL.'?message="The%20transaction%20was%20saved"');
      exit;
    }
  }
  catch (\CreditCommons\Exceptions\CCError $e) {
    $message = $e->getMessage();
  }
}
// Render the transaction form
if (!$_POST or !empty($message) or $message = @$_GET['message']) {
  ?>
  <h1>Remote Bill</h1>
  <div><?php print $message; ?></div>
    <form method="post">
      Payer:<font color="red">*</font> <input type="textfield" name="2ndparty" placeholder="exchange/accID" pattern= "([a-z0-9\-]+\/)+[a-z0-9\-]+" class="<?php print $secondparty_class; ?>" value="<?php print @$_POST['2ndparty']; ?>"><br />
      Amount:<font color="red">*</font> <strong>U</strong><input type="number" name="quant" min="1" class="<?php print $quant_class; ?>" value="<?php print @$_POST['quant']; ?>"><br />
      Description:<font color="red">*</font> <input type="textfield" name="description" class="<?php print $desc_class; ?>" value="<?php print @$_POST['description']; ?>"><br />
      <input type="submit" value="Send">
    </form>
<?php } ?>
<?php /* Put the page footer */ ?>
  </body>
</html>
