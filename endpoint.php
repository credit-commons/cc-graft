<?php
const TRUNKWARD_NODE = 'cctrunk';
const EXCHANGE_RATE = 60;
const NODE_NAME = 'standalone';
const ACC_ID = 'cctrunk';
ini_set('display_errors', '1');

// Exception handler to return credit commons errors.
set_exception_handler('credcom_exceptions');
set_error_handler('credcom_errors');

require_once './vendor/autoload.php';
\CreditCommons\ErrorContext::create(user: ACC_ID, node: NODE_NAME);
require_once('TrunkwardAccount.php');
$trunkward_account = new TrunkwardAccount(TRUNKWARD_NODE, 0, 0); // the zeros here are the min/max balances
authorise($trunkward_account);
/**
 * Mini REST server for basic Credit Commons responses.
 */
// This assumes that there is no server config to mask the php file.
// e.g. The endpoint called is endpoint.php/transaction/relay

switch($_SERVER['PATH_INFO']) {
  case '/transaction/relay':
    if (strtolower($_SERVER['REQUEST_METHOD']) <> 'post') {
      header('Status: 405 Method Not Allowed');
      exit;
    }
    $transaction_data = json_decode(file_get_contents("php://input"));
    foreach ($transaction_data->entries as &$entry) {
      $entry->quant /= EXCHANGE_RATE;
    }
    // Convert to a transaction object and build and validate.
    $transaction = CreditCommons\Leaf\TransversalTransaction::createFromJsonClass($transaction_data, []);
    $new_entries = [];
    /**
     * Now you have to write your own function:
     * Add any fees to the transaction by adding another 'entry'
     * Validate the transaction by checking the min limit on the payer account and max limit on the payee account.
     * If validation fails:
     *   create an error object e.g. $error = new CreditCommons\exceptions\MaxLimitViolation(diff: $excess*EXCHANGE_RATE, acc: $acc_id)
     *   and exit_with($error)
     * If validation succeeds:
     *   save it.
     *   Prepare an array of new entries for the response, remembering to multiply the quants by the EXCHANGE_RATE.
     */
    exit_with($new_entries);
  case '/transaction':
    if (strtolower($_SERVER['REQUEST_METHOD']) <> 'patch') {
      header('Status: 405 Method Not Allowed');
      exit;
    }
    print_r($_REQUEST);
    break;
  case '/handshake':
    if (strtolower($_SERVER['REQUEST_METHOD']) <> 'get') {
      header('Status: 405 Method Not Allowed');
      exit;
    }
    break;
}

function authorise(\CreditCommons\AccountRemoteInterface $trunkward_account) {
  if ($_SERVER['HTTP_CC_USER'] <> $trunkward_account->id) {
    exit_with(new \CreditCommons\Exceptions\DoesNotExistViolation(type: 'account', id: $_SERVER['HTTP_CC_USER']), 400);
  }
  if ($_SERVER['HTTP_CC_AUTH'] == $trunkward_account->getLastHash()) {
    exit_with(new \CreditCommons\Exceptions\PasswordViolation(), 400);
  }
  // @todo return the proper CreditCommons\Exceptions\PasswordViolation
  header('Status: 401 Unauthorised');
}

function exit_with($body = NULL) {
  if ($body instanceOf \CreditCommons\Exceptions\CCViolation) {
    header('HTTP/1.1: 400 CLient error');
    header('Content-Type: application/json');
    $data = ['errors' => [$body]];
  }
  elseif ($body instanceOf \CreditCommons\Exceptions\CCFailure) {
    header('HTTP/1.1: 500 Server error');
    header('Content-Type: application/json');
    $data = ['errors' => [$body]];
  }
  elseif (is_array($body)) {// POST transaction/relay
    header('HTTP/1.1: 200 OK');
    header('Content-Type: application/json');
    $data = ['data' => $body];
  }
  elseif (is_null($body)) {
    header('HTTP/1.1: 201 Saved');
  }
  else {
    die('invalid response');
  }
  print json_encode($data);
  exit;
}

function credcom_exceptions(\Throwable $ex) {
  header('Content-Type: application/json');
  $err = CCError::convertException($ex);
  exit_with($err);
}
function credcom_errors($severity, $message, $filename, $lineno) {
  $trace = array_slice(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS), 1);
  $err = new \CreditCommons\Exceptions\CCFailure($message);
  $err->trace = $trace;
  $err->break = $filename . ':'. $lineno;
  exit_with($err);
}