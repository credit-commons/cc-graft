Graft your platform to the credit commons.

A minimal twig implementation.

The credit commons is visualised as a tree in which every ledger accounts with one unit of account for one community. The user accounts are 'leaves', and the outermost ledgers or nodes are 'twigs', and every node is connected to every other node via a 'trunkward' node.
An existing community may or may not be doing accounting already between its members, but if it wants to connect to a credit commons tree as a twig and do 'transversal' transactions, there's an absolute minimum set of functions it needs to do, namely:
- provide a form for a user to create a transaction
- respond to the creation of a transaction from elsewhere in the tree
- provide a form to move a transaction between workflow states.
- respond to the a transversal transaction changing workflow state.
- prove that its record of transversal transactions corresponds with the record on its trunkward node, which also serves for authentication.
The credit commons protocol allows for 'workflows' to propagate through the tree so that every node spanned by a transaction must recognise and support that workflow. The workflow defines:
- the states it is possible for the transaction to be in
- which actors can move the transaction between states.
- whether a confirmation is needed on transaction creation.
The absolute minimal implementation would assume a workflow with confirmation from neither the creator nor the counterparty, which is to say, a transaction that goes immediately into the 'completed' state.
This package provides an 'are you sure' page for the user, after the transaction has been validated by the tree and any fees added, but then assumes the transaction is saved into the completed state without confirmation from the counterparty.

Two php scripts are provided here. The first builds a form for the user, submits it to the trunkward node, presents the user with an are you sure page, and if the user is sure, confirms it to the trunkward node and redirects to a page of the developer's choosing. The second script provides the credit commons endpoints for when the trunkward node needs to relay information to the twig. That's 3 end points:
POST /transaction/relay
PATCH /transaction/{uuid}/target_state
GET /handshake.

The minimum implementation therefore is mostly about writing to the database and making the form visually consistent with the rest of the site. The transaction form could be split into different files.
Note that most of the heavy lifting is done by a library called cc-php-lib, which is made available through composer, a php package manager. Composer should ideally be installed on the machine, then, from within the directory of your scripts, you run 'composer update --nodev' at the command line and it will fetch the library and organise the namespaces to access that library. Your script need only include the line 
  require_once './vendor/autoload.php';
and then the library objects are under the namespace CreditCommons, e.g.
$new_transaction = \CreditCommons\NewTransaction::create();
The developer will need to translate the transaction objects from the credit commons format to their own system's format for storage. There is also the additional need to store the latest hash, or preferably a whole chain of hashes, in the database or in a file.



